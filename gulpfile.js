var chalk = require('chalk');
var gulp = require('gulp');
var rev = require('gulp-rev');
var revCollector = require('gulp-rev-collector');
var del = require('del');
var runSequence = require('run-sequence');

gulp.task('clean', function() {
	return del(['./dist/*']);
});

gulp.task('css', function () {
	return gulp.src('src/css/*.css')
		.pipe(rev())
		.pipe(gulp.dest('dist/css'))
		.pipe(rev.manifest())
		.pipe(gulp.dest('rev/css'));
});

gulp.task('js', function () {
	return gulp.src('src/js/*.js')
		.pipe(rev())
		.pipe(gulp.dest('dist/js'))
		.pipe(rev.manifest())
		.pipe(gulp.dest('rev/js'));
});

gulp.task('rev', function () {
    return gulp.src(['rev/**/*.json', 'src/**/*.html'])
        .pipe( revCollector({
            replaceReved: true,
            dirReplacements: {
                'css/': './css',
                'js/': './js/'
                // 'cdn/': function(manifest_value) {
                //     return '//cdn' + (Math.floor(Math.random() * 9) + 1) + '.' + 'exsample.dot' + '/img/' + manifest_value;
                // }
            }
        }) )
        .pipe( gulp.dest('dist') );
});

gulp.task('clean-rev', function() {
	return del(['./rev']);
});

gulp.task('default', function() {
	runSequence('clean', 'js', 'css', 'rev', 'clean-rev', function() {
		console.log(chalk.black.bgCyan('Finished'));
	});
});